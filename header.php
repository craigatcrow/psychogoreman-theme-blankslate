<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" href="https://use.typekit.net/iyj4cdo.css">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header">
<div id="branding">
<div id="site-title">
<h1><a href="/"><img src="/wp-content/themes/blankslate/images/logo.png" alt="PG Psycho Goreman"></a></h1>
</div>

</div>
</header>
<div id="container">