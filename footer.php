</div>
<footer id="footer">
<nav id="menu">
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>
<ul class="social-menu">

  <li><a href="https://www.facebook.com/PsychoGoreman" title="Follow us on Facebook">
  <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="svg-container">
  <g>
    <title>Follow us on Facebook</title>
    <rect class="social bg" width="100" height="100" />
    <g class="icon-group">
    <path class="social icon"
      d="M57.75,100V54.4h15.3l2.3-17.8H57.75V25.2c0-5.1,1.5-8.7,8.8-8.7H76V.7A129.62,129.62,0,0,0,62.25,0c-13.5,0-22.8,8.3-22.8,23.5V36.6H24.05V54.4h15.4V100Z" />
</g>
  </g>
</svg>
</a></li>

  <li><a href="https://www.instagram.com/psychogoreman/" title="Follow us on Instagram">
  <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="svg-container">

<g>
  <title>Follow us on Instagram</title>
  <rect class="social bg" width="100" height="100" />
  <g>
    <rect class="social icon" x="73.15" y="11.6" width="15.3" height="15.3" rx="3.8" />
    <g class="icon-group">
      <path class="social icon"
      d="M50.05,35.4a14.16,14.16,0,0,0-7.7,2.3,4.68,4.68,0,0,1,2.3-.4,7.3,7.3,0,0,1,0,14.6,7.62,7.62,0,0,1-7.3-7.3,4.68,4.68,0,0,1,.4-2.3,13.22,13.22,0,0,0-2.3,7.7,14.6,14.6,0,1,0,14.6-14.6" />
    <path class="social icon"
      d="M81.25,0H18.85A18.91,18.91,0,0,0,.05,18.8V81.1A18.93,18.93,0,0,0,18.85,100h62.3A18.84,18.84,0,0,0,100,81.2V18.8A18.76,18.76,0,0,0,81.25,0ZM72.87,46.4c0,.4.11.79.15,1.2,0,.19,0,.38,0,.58,0,.6.08,1.21.08,1.82A23.1,23.1,0,0,1,27,50h0a23.66,23.66,0,0,1,4.1-13.07A23.07,23.07,0,0,1,72.87,46.4ZM92.45,34.6H76.67a30.72,30.72,0,0,0-53.26,0H7.75V18.8A11.18,11.18,0,0,1,19,7.6h62.3a11.18,11.18,0,0,1,11.2,11.2Z" />
</g>
  </g>
</g>
</svg>
</a></li>

  <li><a href="https://twitter.com/PsychoGoreman" title="Follow us on Twitter">
  <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="svg-container">
  <g>
    <title>Follow us on Twitter</title>
    <rect class="social bg" width="100" height="100" />
    <g class="icon-group">
      <path class="social icon"
      d="M31.57,90.61c37.66,0,58.34-31.27,58.34-58.34a24.37,24.37,0,0,0-.1-2.7A41,41,0,0,0,100,19a41.93,41.93,0,0,1-11.79,3.2,20.62,20.62,0,0,0,9-11.29,42.38,42.38,0,0,1-13,5,20.5,20.5,0,0,0-35.47,14,24.16,24.16,0,0,0,.5,4.7A58.3,58.3,0,0,1,7,13.19,20.06,20.06,0,0,0,4.2,23.48a20.56,20.56,0,0,0,9.09,17.08A19.58,19.58,0,0,1,4,38v.3A20.56,20.56,0,0,0,20.48,58.34a19,19,0,0,1-5.4.7,17.19,17.19,0,0,1-3.89-.4A20.34,20.34,0,0,0,30.37,72.83,41.2,41.2,0,0,1,4.9,81.62a39.08,39.08,0,0,1-4.9-.3,58.48,58.48,0,0,0,31.57,9.29" />
</g>
  </g>
</svg>
</a></li>

  <li><a href="https://www.youtube.com/channel/UC0aDWY1t7wvQmt6KjDjUUJw" title="watch us on YoutTube">
  <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="svg-container">

  <g>
    <title>Watch us on YouTube</title>
    <rect class="social bg" width="100" height="100" />
    <g class="icon-group">
      <path class="social icon"
        d="M86.6,0H13.3A13.36,13.36,0,0,0,0,13.3V86.7A13.36,13.36,0,0,0,13.3,100H86.7A13.38,13.38,0,0,0,100,86.6V13.3A13.38,13.38,0,0,0,86.6,0Zm-8,64.6a6.6,6.6,0,0,1-6,5.7,380.35,380.35,0,0,1-45.3,0,6.79,6.79,0,0,1-6-5.7,143.46,143.46,0,0,1,0-29.3,6.79,6.79,0,0,1,6-5.7,380.35,380.35,0,0,1,45.3,0,6.79,6.79,0,0,1,6,5.7A143.46,143.46,0,0,1,78.6,64.6Z" />
      <polygon class="social icon" points="39.65 39.19 60.35 49.93 39.65 60.81 39.65 39.19" />
    </g>
  </g>
</svg>
</a></li>
<li><a href="mailto:info@psychogoreman.com" title="Send us an email">
<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" class="svg-container">


  <g class="icon-group">
    <title>send us an email</title>
    <rect class="social bg" width="100" height="100" />
    <g>
      <path class="social icon"
        d="M99.49,66,63.89,46.4l-7.07,6.78A11.26,11.26,0,0,1,45.61,56l-8.91-3.1L14,87.37a5.39,5.39,0,0,0,5,1.25L95.65,69.47A5.39,5.39,0,0,0,99.49,66Z" />
      <polygon class="social icon" points="1.98 41.82 12.08 82.21 32.27 51.48 1.98 41.82" />
      <path class="social icon"
        d="M.73,36.81l46.65,15a6,6,0,0,0,6.19-1.54l34.2-33.61-.44-1.77a4.88,4.88,0,0,0-6.19-3.46L4.49,30.55a5,5,0,0,0-3.83,6Z" />
      <polygon class="social icon" points="98.75 60.55 89.02 21.64 67.13 43.08 98.75 60.55" />
    </g>
  </g>
</svg>
</a>
</li>

</ul>
</footer>
</div>
<?php// wp_footer(); ?>
</body>
</html>